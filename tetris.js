var canvas = document.getElementById('gameMap');
const ctx = canvas.getContext('2d');
const ctx1 = canvas.getContext('2d');
// const keyCode = require('keycode');

const keyPress = document.getElementById('keyPress');

document.addEventListener('keydown', logKey);

function logKey(e) {
  let key = e.keyCode;
  switch(key) {
      case(65): {
          moveLeft(block);
      }
      case(68): {
          moveRight(block);
      }
      case(83): {
          pressed = true;
          jumpDown();
      }
      case(87): {
          rotate(block);
      }
  }
}

//--- the map
var score = 0;
var full = true;
var moving = false;
const us = 35;
var map = [];
const row = 25;
const column = 10;
var currentColor = '';
var nextColor = '';
var preMatrix = [];

function baseMap() {
    for (var y = 0; y < row; y++) {
        map.push([]);
        for (var x = 0; x < column; x++) {
            map[y].push(0);
        }
    }
}

function createMap(matrix) {
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, matrix[0].length * us, matrix.length * us);
    ctx.strokeRect(0, 0, matrix[0].length * us, matrix.length * us);
    for (var y = 0; y < row; y++) {
        map.push([]);
        for (var x = 0; x < column; x++) {
            map[y].push(0);
        }
    }
}

function createBorder(matrix) {
    for (y in matrix) {
        ctx.fillStyle='grey';
        ctx.fillRect(0, y * us, us, us);
        ctx.fillRect(us + matrix[0].length * us, y * us, us, us);
        ctx.strokeRect(0, y * us, us, us);
        ctx.strokeRect(us + matrix[0].length * us, y * us, us, us)
    }
    for (var x = 0; x < matrix[0].length + 2; x++) {
        ctx.fillStyle='grey';
        ctx.fillRect(x * us, 0, us, us);
        ctx.fillRect(x * us, matrix.length * us, us, us);
        ctx.strokeRect(x * us, 0, us, us);
        ctx.strokeRect(x * us, matrix.length * us, us, us);
    }
}

//--- the blocks

var T = [
    [0, 0, 0],
    [1, 1, 1],
    [0, 1, 0]
]

var I = [
    [0, 1, 0],
    [0, 1, 0],
    [0, 1, 0]
]

var J = [
    [0, 0, 0],
    [1, 0, 0],
    [1, 1, 1]
]

var L = [
    [0, 0, 0],
    [0, 0, 1],
    [1, 1, 1]
]

var S = [
    [0, 0, 0],
    [0, 1, 1],
    [1, 1, 0]
]

var Z = [
    [0, 0, 0],
    [1, 1, 0],
    [0, 1, 1]
]

var O = [
    [0, 0, 0],
    [0, 1, 1],
    [0, 1, 1]
]

// var I2 = [
//     [0, 1, 0],
//     [0, 1, 0],
//     [0, 1, 0],
//     [0, 1, 0]
// ]
const blockList = [L, J, S, Z, I, T, O]
const colorList = [
    'red',
    'green',
    'blue',
    'yellow',
    'purple',
    'cyan',
    'orange'
]

for (shape in blockList) {
    for (y in blockList[shape]) {
        for (var add = 0; add < column / 2 - 2; add++) {
            blockList[shape][y].unshift(0);
        }
    }
}

//--- the score-board --- all things about board

function getPoint(y) { //--- print when player got a point 
    ctx.fillStyle = 'red';
    ctx.font = '24px Aria';
    ctx.fillText('Got a point right here !', column * us + 3 * us, y * us + us/2);
}

function deleteGetPoint(y) { //--- delete old notification
    ctx.fillStyle = 'white';
    ctx.font = '24px Aria';
    ctx.fillText('Got a point right here !', column * us + 3 * us, y * us + us/2);
}

// ctx.fillStyle = 'grey';
// ctx.fillRect(column * us + 4 * us, us, 6 * us, 4 * us)
// ctx.fillStyle = 'black';
// ctx.fillRect(column * us + 4 * us + us/4, us + us/4, 6 * us - us/2, 4 * us - us/2)
// ctx.fillStyle = 'white';
// ctx.font = "30px Georgia";
// ctx.fillText('SCORE-BOARD', column * us + 4.8 * us, 2.5 * us, 300)
// ctx.fillStyle = 'white';
// ctx.font = "26px Aria";
// ctx.fillText('Score: ' + score, column * us + 4.8 * us, 3 * us, 300)

function updateScore() { //--- this will create and update the score board
    ctx.fillStyle = 'grey';
    ctx.fillRect(column * us + 4 * us, us, 6 * us, 4 * us)
    ctx.fillStyle = 'black';
    ctx.fillRect(column * us + 4 * us + us/4, us + us/4, 6 * us - us/2, 4 * us - us/2)
    ctx.fillStyle = 'white';
    ctx.font = String(us * 3 / 5) + "px Georgia";
    ctx.fillText('SCORE-BOARD', column * us + 4.8 * us, 2.5 * us, us * 6)
    ctx.fillStyle = 'white';
    ctx.font = "Italic " + String(us / 2) + "px Aria";
    ctx.fillText('Score: ' + score, column * us + 4.8 * us, 3.2 * us, us * 6)
}

function nextBlock(matrix) {
    ctx.fillStyle = 'red';
    ctx.fillRect(column * us + 4 * us, us + 5 * us, 6 * us, 6.5 * us)
    ctx.fillStyle = 'black';
    ctx.fillRect(column * us + 4 * us + us/4, us  + 5 * us + us/4, 6 * us - us/2, 6.5 * us - us/2)
    ctx.fillStyle = 'red';
    ctx.font = "bold " + String(us * 3 / 5) + "px Georgia";
    ctx.fillText('NEXT BLOCK', column * us + 4.8 * us, 2.5 * us + 4.5 * us, us * 6)
    let baseX = column * us + 4 * us + us/4 - 1.7 * us;
    let baseY = 6 * us + us/4 + 1.2 * us;
    for (var y = matrix.length - 3; y < matrix.length; y++) {
        for (x in matrix[y]) {
            if (matrix[y][x] == 1) {
                ctx.fillStyle = nextColor;
                ctx.strokeRect(baseX + x * us, baseY + y * us, us, us);
                ctx.fillRect(baseX + x * us, baseY + y * us, us, us);
                ctx.fillStyle = 'black';
                ctx.strokeRect(baseX + x * us + us/8, baseY + y * us + us/8, us - us/4, us - us/4);
                ctx.fillRect(baseX + x * us + us/8, baseY + y * us + us/8, us - us/4, us - us/4);
            }
        }
    }
}
//--- the functions --- 

//--- FUNCTION FOR THE VISUAL 

function getRandomColour(){
    var red = Math.floor(Math.random()* 255);
    var green = Math.floor(Math.random() * 255);
    var blue = Math.floor(Math.random() * 255);
  
    return "rgb("+red+","+green+"," +blue+" )";  
}


function effect(matrix) { //--- later
    for (var y = 0; y < matrix.length; y++) {
        for (x in matrix[y]) {
            if (y < row) {
                if (matrix[y][x] == 2) {
                    // if (y * us = 0) {
                        ctx.fillStyle='green';
                        ctx.fillRect(x * us, y * us, us, us);
                        ctx.strokeRect(x * us, y * us, us, us);
                    // 
                    matrix[y][x] = 0;
                };
            }
        };
    };
}

function draw(matrix) { //--- draw the block
    if (matrix == map) { color = 'red' };
    if (matrix == preMatrix) { color = 'white'}
    if (matrix == block) { color = currentColor; }
    if (matrix == preMatrix) {
        for (y in matrix) {
            for (x in matrix[y]) {
                if (matrix[y][x] == 1) {
                    ctx.fillStyle = color;
                    ctx.fillRect(us + x * us, y * us, us, us);
                    ctx.strokeRect(us + x * us, y * us, us, us);
                    ctx.fillStyle = 'black';
                    ctx.fillRect(us + x * us +us/16, y * us + us/16, us - us/8, us - us/8);
                    ctx.strokeRect(us + x * us + us/16, y * us + us/16, us - us/8, us - us/8);
                };
            };
        };
    }
    else {
        for (y in matrix) {
            for (x in matrix[y]) {
                if (matrix[y][x] == 1) {
                    ctx.fillStyle = 'black';
                    ctx.fillRect(us + x * us, y * us, us, us);
                    ctx.strokeRect(us + x * us, y * us, us, us);
                    ctx.fillStyle = color;
                    ctx.fillRect(us + x * us +us/16, y * us + us/16, us - us/8, us - us/8);
                    ctx.strokeRect(us + x * us + us/16, y * us + us/16, us - us/8, us - us/8);
                };
            };
        };
    }
}

function clear(matrix) { //--- kill the old block
    for (var y = 0; y < matrix.length; y++) {
        for (x in matrix[y]) {
            if (y < row) { 
                if (matrix[y][x] == 0) {
                    // if (y * us = 0) {
                        ctx.fillStyle='black';
                        ctx.fillRect(us + x * us, y * us, us, us);
                        // ctx.strokeRect(x * us, y * us, us, us);
                    // 
                };
            }
        };
    };
}

//--- FUNCTION FOR THE GAME LOGIC

function updateMap(y) { //--- update the whole map when player get point
    if (full == true) {
        for (y; y > 0; y--)
            for (x in map[y]) {
                if (map[y][x] == 0 && map[y-1][x] == 1) {
                    map[y][x] = 1;
                    map[y-1][x] = 0;
                } 
                // if (map[y][x] == 1 && map[y-1][x] == 0) {
                //     map[y][x] = 0;
                //     map[y-1][x] = 1;
                // }
            }
        clear(map);
    }
}

function check() { //--- check if player get point
    let streak = 0;
    var getScore = false;
    if (getScore == false) {
        for (line in map) {
            deleteGetPoint(line);
        }
    }
    for (y in map) {
        full = true;
            for (x in map[y]) {
                if (map[y][x] == 0) {
                    full = false;
                    break;
                }
            }
        if (full == true) {
            score += streak;
            score++;
            streak++;
            for (x in map[y]) {
                map[y][x] = 0;
            }
            getPoint();
            getScore = true;
            updateMap(y);
        } 
        clear(map);
    }
    
}

function combine(matrix) { //--- for game logic, when block has its place, merge it into the map
    if (moving == false) {
        for (y in matrix) {
            for (x in matrix[y]) {
                if (matrix[y][x] == 1 && map[y][x] == 0) {
                    map[y][x] = 1;
                }
                // if (matrix[y][x] == 0 && map[y][x] == 1) {
                //     map[y][x] = 0;
                // }
            }
        }
    }
}

function create() { //--- create random block
    currentColor = nextColor;
    arr = [];
    let a;
    a = Math.floor(Math.random()*(blockList.length));
    nextColor = colorList[a];
    for (y in blockList[a]) {
        arr.push([]);
        for (x in blockList[a][y]) {
            if (map[y][x] == 1) { 
                lose = true;
                return;
            }; 
            arr[y][x] = blockList[a][y][x];
        }
    }
    return arr
}

function clean(matrix) { //--- if last element is full of 0, delete it 
    let able = true;
    for (n in matrix[matrix.length-1]) {
        if (matrix[matrix.length-1][n] == 1) {
            able = false;
        }
    }
    if (able) {
        matrix.pop();
    }
}

//--- MOVING AND ROTATING PART

function preBlock(matrix) {
    
    //--- the array: add to preMatrix if it's not the max
    let arr = [];
    for (var i = 0; i < matrix[0].length; i++) {
        arr.push(0);
    }

    //---create the preMatrix = matrix first
    preMatrix = [];
    var max = false;
    for (var y = 0; y < matrix.length; y++) {
        preMatrix.push([]);
        for (var x = 0; x < matrix[y].length; x++) {
            preMatrix[y].push(matrix[y][x]);
        }
    }

    //--- conditions to get the complete preMatrix
    for (var n = 0; n < row - 3; n++) {
        max = false;
        for (var y = 0; y < preMatrix.length; y++) {
            for (var x = 0; x < preMatrix[y].length; x++) {
                if (preMatrix[y][x] == 1) {
                    if (map[y+1][x] == 1 || map[y+1][x] == undefined || y + 1 > 24) {
                        max = true;
                        break;
                    }
                }
            }
            if (max == true) { break; } 
        }
        if (max == false) {
            preMatrix.unshift(arr);
        }
        else {
            if (max == true) {
                break;
            }
        } 
    }
}

function moveDown(matrix) { //--- moving down
    draw(matrix);
    let arr = [];
    let able = true;
    for (var i = 0; i < matrix[0].length; i++) {
        arr.push(0);
        if (matrix.length < row) {
            if (map[matrix.length][i] == 1 && matrix[matrix.length - 1][i] == 1 ||
                map[matrix.length - 1][i] == 1 && matrix[matrix.length - 2][i] == 1 ||
                map[matrix.length - 2][i] == 1 && matrix[matrix.length - 3][i] == 1 
            ) {
                able = false;
                break;
            };
        };
    }
    if (matrix.length < row) {
        if (able == true) {
            matrix.unshift(arr);
            moving = true;
        } else { moving = false}
    } else { moving = false }
    draw(preMatrix);
}

function moveRight(matrix) { //--- moving right
    if (moving == true) {
        let able = true;
        for (var y = matrix.length - 3; y < matrix.length; y++) {
            for (var x = matrix[y].length - 3; x < matrix[y].length; x++) {
                if (matrix[y][x] == 1) {
                    if (map[y][x + 1] == 1 || map[y][x + 1] == undefined) {
                        able = false;
                    };  
                }; 
            };        
        };
        if (able) {
            if (matrix[0].length < column) {
                for (y in matrix) {
                    matrix[y].unshift(0);
                };
            }
            else if (matrix[0].length = column) {
                for (y in matrix) {
                    if (matrix[y][column - 1] == 1) {
                        able = false;
                    };
                };
                if (able) {
                    for (y in matrix) {
                        matrix[y].unshift(0);
                    };
                };
            };
        };
        preBlock(matrix);
        draw(preMatrix);
    };
    clear(map);
    draw(matrix);
}

function moveLeft(matrix) { //--- moving left
    if (moving == true) {
        let able = true; 
        for (var y = matrix.length - 3; y < matrix.length; y++) {
            for (var x = matrix[y].length - 3; x < matrix[y].length; x++) {
                if (matrix[y][x] == 1) {
                    if (map[y][x - 1] == 1 || map[y][x - 1] == undefined) {
                        able = false;
                        break;
                    };
                };
            };
        };
        if (able == true) {
            for (y in matrix) {
                matrix[y].shift();
            }
        }
        preBlock(matrix);
        draw(preMatrix);
    }
    clear(map);
    draw(matrix);
}

function rotate(matrix) { //--- rotate
    let can = true;
    for (var y = matrix.length - 3; y < matrix.length; y++ ) {
        for (var x = matrix[y].length - 3; x < matrix[y].length; x++) {
            if (map[y][x] == 1 || map[y][x] == undefined) {
                can = false;
                break;
            }
        }
    }
    if (can == true && moving == true) {
        if (matrix.length < row && matrix[0].length < column + 1) {
            let rotated = []; //--- the rotated array
            for (y in matrix) { //---set up
                let arr = [];
                for (x in matrix[y]) {
                    arr.push(0);
                }
                if (y < matrix.length - 3) {
                    rotated.unshift(arr);
                }
            }
            
            for (var i = 3; i > 0 ; i--) {
                rotated.push([]);
                for (var j = 0; j < matrix[0].length - 3;j++) {
                    rotated[matrix.length - i].push(0);
                }
            }
            //---rotate part
            let index = matrix.length;
            j = 1;
            for (var y = matrix.length - 3; y < matrix.length; y++) {
                i = 3; 
                for (var x = matrix[y].length - 3; x < matrix[y].length; x++) {
                    rotated[index - i].push(matrix[index - j][matrix[y].length - i]);
                    i--;
                }
                j++;
            }
            //---the block rotate as the rotated array
            for (var y = matrix.length - 3; y < matrix.length; y++) {
                for (var x = matrix[y].length - 3; x < matrix[y].length; x++) { 
                    matrix[y][x] = rotated[y][x];
                }
            } 
        }
    } 
    clear(map);
    draw(matrix);
    preBlock(matrix);
    draw(preMatrix);
}

function jumpDown() {
        block = [];
        for (y in preMatrix) {
            block.push([]);
            for (x in preMatrix[[y]]) {
                block[y].push(preMatrix[y][x]);
            }
        }
        moving = false;
        draw(block);
        combine(block);
        pressed = false;
}

function restart() {
    for (var y in block) {
        for (var x in block[y]) {
            block[y][x] = 0;;
        }
    }
    score = 0;
    full = true;
    moving = false;
    const us = 50;
    const row = 20;
    const column = 10;
    map = [];
    baseMap()
    clear(map);
    createBorder(map);
    currentColor = '';
    nextColor = '';
    block = [];
    next = [];
    newBlockCreated = true;
    start = true;
    lose = false;;
    preMatrix = [];
}   

var block = [];
var next = [];
var newBlockCreated = true;
var start = true;
var lose = false;
// function pressKey(event) {
//     let x = event.keyCode;
//     switch(x) {
//         case(a): {
//             moveLeft(block);
//         }
//         case(39): {
//             moveRight(block);
//         }
//     }
// }

// var currentMap; //--- for the future
// var lose;
// next = create();
baseMap();
setInterval(() => {
    clear(map);
    createBorder(map);
    if (lose != true) {
        if (start == true) {
            next = create();
            // getColor(next);
            start = false;
        }
        if (moving == false) {
            // getColor(block);
            
            draw(block);
            combine(block);
            check();
            updateScore();
            draw(block);
            
            //--- create new block here
            
            block = [];
            for (y in next) {
                block.push([]);
                for (x in next[y]) {
                    block[y].push(next[y][x]);
                }
            }
            preBlock(block);
            newBlockCreated = false;
        }
        if (newBlockCreated == false) {
            next = create();
            newBlockCreated == true;
        }
        moveDown(block);
        if (moving == true) { 
            
            
            clean(block);
            clear(map);
            createBorder(map);
            draw(preMatrix);
            
            newBlockCreated = true;
            nextBlock(next);
            draw(block);
            
            
            // draw(block);
        }
    } 
    else { 
            ctx.fillStyle = 'black';
            ctx.fillRect(0, 0, column * us + 2 * us, row * us + 2 * us);
            ctx.fillStyle = 'white';
            ctx.font = 'bold italic ' + us * 8 / 5 + 'px Arial';
            ctx.fillText('GAME OVER !', us * 4, us * 6, us * 4);
    }

},700)